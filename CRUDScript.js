const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
const gColName = ["id", "courseName", "price", "discountPrice", "duration", "level",
    "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending"];

const gCOURSES_ID_COL = {
    ID: 0,
    COURSES_NAME: 1,
    PRICE: 2,
    DISCOUNT: 3,
    DURATION: 4,
    LEVEL: 5,
    COVER_IMAGE: 6,
    TEACHER_NAME: 7,
    TEACHER_PHOTO: 8,
    IS_POPULAR: 9,
    IS_TRENDING: 10,
    ACTION: 11,
}
class Main {
    constructor() {
        $(document).ready(() => {
            this.modal = new Modal()
            this.table = new RenderTable()
            this.table.renderTable()
            $('.toast').toast();
            $("#add-courses").on("click", () => {

                this.modal.openModal(undefined,"add/create")
            })

        })
    }

}

const main = new Main()
class RenderTable {
    courseTable
    constructor() {
        this.api = new CallApi()
        this.modal = new Modal()
    }

    _getCourseList() {
        this.api.onGetCoursesListClick((courses) => {
            this._renderCourses(courses)

        })

    }
    _renderCourses(courses) {
        if ($.fn.DataTable.isDataTable('#courses-table')) {
            $('#courses-table').DataTable().destroy();
        }

        this.courseTable = $("#courses-table").DataTable({
            columns: [
                { data: gColName[gCOURSES_ID_COL.ID] },
                { data: gColName[gCOURSES_ID_COL.COURSES_NAME] },
                { data: gColName[gCOURSES_ID_COL.PRICE] },
                { data: gColName[gCOURSES_ID_COL.DISCOUNT] },
                { data: gColName[gCOURSES_ID_COL.DURATION] },
                { data: gColName[gCOURSES_ID_COL.LEVEL] },
                { data: gColName[gCOURSES_ID_COL.COVER_IMAGE] },
                { data: gColName[gCOURSES_ID_COL.TEACHER_NAME] },
                { data: gColName[gCOURSES_ID_COL.TEACHER_PHOTO] },
                { data: gColName[gCOURSES_ID_COL.IS_POPULAR] },
                { data: gColName[gCOURSES_ID_COL.IS_TRENDING] },
                { data: gColName[gCOURSES_ID_COL.ACTION] },
            ],
            columnDefs: [
                {
                    targets: gCOURSES_ID_COL.ID,
                    render: (data, type, row, meta) => {
                        if (type === 'display') {
                            return meta.row + 1;
                        } else {
                            return data;
                        }
                    }
                },
                {
                    targets: gCOURSES_ID_COL.COVER_IMAGE,
                    render: function (data) {
                        return `<img src="${data}" style="height: 90px; width:100px; object-fit:cover">`;
                    }
                },
                {
                    targets: gCOURSES_ID_COL.TEACHER_PHOTO,
                    render: function (data) {
                        return `<img src="${data}" " style="height:100px; width:85px;  object-fit:cover">`;
                    }
                },
                {
                    targets: gCOURSES_ID_COL.ACTION,
                    defaultContent: ` 
                                        <button class='btn btn-sm btn-primary px-4 mb-2  edit'>Sửa</button>
                                       <button class='btn btn-sm  btn-danger px-4 delete'>Xóa</button>
                                   `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOURSES_ID_COL.ACTION) {
                            $(cell).on("click", ".edit", () => {
                                const id = this.courseTable.row(rowIndex).data().id;
                                this.modal.openModal(id,"add/create")
                            })

                            $(cell).on("click", ".delete", () => {
                                const id = this.courseTable.row(rowIndex).data().id;
                                this.modal.openModal(id, "delete")
                            })
                        }

                    }

                }
            ],
        })

        this.courseTable.clear()
        this.courseTable.rows.add(courses)
        this.courseTable.draw()

    }
    renderTable() {
        this._getCourseList()
    }
}

class Modal {
    course = {}
    requiredFields = [
        "course-name",
        "course-code",
        "course-cover",
        "course-duration",
        "course-price",
        "course-level",
        "teacher-name",
        "teacher-image" 
    ]
    constructor() {
        this.api = new CallApi()
    }
    _renderCourseData(id) {
        this.api.onGetCourseByIdClick(id, (course) => {
            $("#course-name").val(course.courseName);
            $("#course-code").val(course.courseCode);
            $("#course-duration").val(course.duration);
            $("#course-price").val(course.price);
            $("#course-discount").val(course.discountPrice);
            $("#course-level").val($("#course-level option").filter(function () {
                return $(this).text().toLowerCase() === course.level.toLowerCase();
            }).val());
            $("#course-cover").val(course.coverImage);
            $("#teacher-name").val(course.teacherName);
            $("#teacher-photo").val(course.teacherPhoto);
            $("#isTrending").prop("checked", course.isTrending);
            $("#isPopular").prop("checked", course.isPopular);

        })
        this._clearInValid()
    }

    _clearForm() {
        $("#course-form").get(0).reset();
        $("#isTrending, #isPopular").prop("checked", false);
    }

    _clearInValid() {
        $(".form-control").removeClass("is-invalid").val("");
        $(".invalid-feedback").remove();
    }

    _getInputValue() {
        $(".form-control").on("input change", () => {
            this.course = {
                courseName: $("#course-name").val(),
                courseCode: $("#course-code").val(),
                duration: $("#course-duration").val(),
                price: $("#course-price").val(),
                discountPrice: $("#course-discount").val(),
                level: $("#course-level option:selected").val(),
                coverImage: $("#course-cover").val(),
                teacherName: $("#teacher-name").val(),
                teacherPhoto: $("#teacher-photo").val(),

            };
        });

        $("#isTrending, #isPopular").on("change", (event) => {
            this.course[event.target.id] = $(event.target).is(":checked");
        });
    }
    _submitForm(id) {
        this._getInputValue()
        $("#submit-form").on("click", () => {
            $(".error-message").remove();
            let isValid = true;
            this.requiredFields.forEach(field => {
                if (!$(`#${field}`).val() || (field === "course-price" && Number($(`#${field}`).val()) < 0)) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Please enter a valid value for ${field.replace("-", " ")}.</div>`);
                } else {
                    $(`#${field}`).removeClass("is-invalid");
                    $(`#${field} + .error-message`).remove();
                }
            });
            if (isValid) {
                if (id) {
                    this.api.onUpdateCourseClick(id, this.course);
                } else {
                    this.api.onCreateCourseClick(this.course)
                }
                this._clearForm();
                $('#course-modal').modal('hide');

            }
        });
    }

    _deleteCourse(id) { 
        $("#deleteCourseBtn").on("click",()=> {

            this.api.onDeleteCourseClick(id)
        })
    }
    openModal(id,type) {
        if(type === "add/create") {

            if (id) {
                this._renderCourseData(id)
                this._submitForm(id)
    
            } else {
                this._clearInValid()
                this._clearForm();
                this._submitForm()
            }
            $('#course-modal').modal('show');
        }
        if(type ==="delete") {
            this._deleteCourse(id)
            $('#deleteCourseModal').modal('show');

        }
    }
}

class CallApi {
    constructor() {
    }

    onShowToast(title, message) {
        $('#myToast .mr-auto').text(title)
        $('#myToast .toast-body').text(message);
        $('#myToast').toast('show');
    }
    onGetCoursesListClick(callback, params) {
        var vAPI_URL = gBASE_URL + `/courses?${params}`;
        $.ajax({
            url: vAPI_URL,
            cache: false,
            method: 'GET',
            dataType: 'json',
            success: function (response) {
                callback(response, params);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    onGetCourseByIdClick(id, callback) {
        const vAPI_URL = gBASE_URL + "/courses/";
        $.ajax({
            url: vAPI_URL + id,
            type: "GET",
            contentType: "application/json",
            success: function (response) {
                callback(response)
            },
            error: function (error) {
                console.log(error);
            }
        })
    }
    onCreateCourseClick(course) {
        const vAPI_URL = gBASE_URL + "/courses";
        $.ajax({
            url: vAPI_URL,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(course),
            success:  (response)=> {
                this.onShowToast("Create Course", `You have created course: ${response.courseName}`)
                const render = new RenderTable()
                render.renderTable()
            },
            error: function (error) {
                console.log(error);
            }
        });
    }


    onUpdateCourseClick(id, course) {

        const vAPI_URL = gBASE_URL + "/courses/";
        $.ajax({
            url: vAPI_URL + id,
            cache: false,
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(course),
            success: (response) => {
                if (response) {
                    this.onShowToast("Update Course", `You have updated course: ${response.courseName}`)
                    const render = new RenderTable()
                    render.renderTable()
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Error callback function
            }
        });
    }
    onDeleteCourseClick(id) {
        const vAPI_URL = gBASE_URL + "/courses/";
        $.ajax({
            url: vAPI_URL + id,
            type: "DELETE",
            success:  ()=> {
                const render = new RenderTable()
                render.renderTable()
                $('#deleteCourseModal').modal('hide');
                this.onShowToast("Delete Course", `You have deleted the course successfully`)

            },
            error: function () {

            }
        });
    }

}