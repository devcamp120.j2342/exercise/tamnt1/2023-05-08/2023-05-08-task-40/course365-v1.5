const gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";

// Main function to load all data
class Main {
  constructor() {
    $(document).ready(()=> {
      this.page = new RenderCourses()
      this.page.renderPage()
      
    })
  }
}
new Main()
// Render function to render all courses
class RenderCourses {
  #popular = "isPopular=true";
  #trending = "isTrending=true"
  constructor() {
    this.api = new CallApi()
  }
  _getAllCourses(filter) {
    const filterQuery = filter ? `${filter}` : '';
    this.api.onGetCoursesListClick((courses, type) => this._renderCourses(courses, type), filterQuery);
  }
  
  _getAllCoursesByRecommending() {
    this._getAllCourses();
  }
  
  _getAllCoursesByTrending() {
    this._getAllCourses(this.#trending);
  }
  
  _getAllCoursesByPopularity() {
    this._getAllCourses(this.#popular);
  }

  _renderCourses(courses, type) {
    const recommendContainer = $(".recommend-container");
    const popularContainer = $(".popular-container");
    const trendingContainer = $(".trending-container");
    courses.slice(0, 4).forEach((course) => {
      const cardHtml = `
        <div class="col-md-3 course" data-course-id="${course.id}">
          <div class="card mb-3 cartList">
            <img class="card-img-top" src="${course.coverImage}" alt="Card image cap">
            <div class="card-body d-flex flex-column">
              <h5 class="card-title text-primary">${course.courseName}</h5>
              <br>
              <p class="card-text flex-grow-1"><i class="far fa-clock"></i> ${course.duration}</p>
              <p class="card-text"><strong>${course.discountPrice}</strong> <del>${course.price}</del> </p>
            </div>
            <div class="card-footer">
              <div class="media">
                <img src="${course.teacherPhoto}" alt="Avatar" class="mr-3 rounded-circle" width="64" height="64">
                <p>${course.teacherName}</p>
                <a href="" class="btn"><i class="far fa-bookmark"></i></a>
              </div>
            </div>
          </div>
        </div>
      `;  
      switch (type) {
        case "":
          recommendContainer.append(cardHtml);
          break;
        case this.#popular:
          popularContainer.append(cardHtml);
          break;
        case this.#trending:
          trendingContainer.append(cardHtml);
          break;
      }
      
    });
  }
   renderPage() {
    $(".recommend-container").empty()
    $(".popular-container").empty()
    $(".trending-container").empty()
    this._getAllCoursesByRecommending()
    this._getAllCoursesByTrending()
    this._getAllCoursesByPopularity()
  }
}
//Call Api to get all courses from databases
class CallApi {
  constructor() {
  }
  onGetCoursesListClick(callback, params) {
    var vAPI_URL = gBASE_URL + `/courses?${params}`;
    $.ajax({
      url: vAPI_URL,
      cache: false,
      method: 'GET',
      dataType: 'json',
      success: function (response) {
        callback(response, params);
      },
      error: function (error) {
        console.log(error);
      }
    });
  }
}